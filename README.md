## Lab 7
by m.nicholson@innopolis.university

### Part 1

* set up table:

```sql
CREATE DATABASE lab;
CREATE TABLE Player (username TEXT UNIQUE, balance INT CHECK(balance >= 0));
CREATE TABLE Shop (product TEXT UNIQUE, in_stock INT CHECK(in_stock >= 0), price INT CHECK (price >= 0));
INSERT INTO Player (username, balance) VALUES ('Alice', 100);
INSERT INTO Player (username, balance) VALUES ('Bob', 200);
INSERT INTO Shop (product, in_stock, price) VALUES ('marshmello', 10, 10);
```

### Part 2
```sql
CREATE TABLE Inventory (
    username TEXT REFERENCES Player(username) NOT NULL,
    product  TEXT REFERENCES Shop(product) NOT NULL,
    amount   INT CHECK (amount >= 0),
    UNIQUE(username, product)
);

CREATE OR REPLACE FUNCTION check_inventory_limit_size() RETURNS TRIGGER AS $$
DECLARE
    total_sum_amount INT;
BEGIN
    SELECT SUM(amount) INTO total_sum_amount FROM Inventory WHERE username = NEW.username;
    IF total_sum_amount > 100 THEN
        RAISE EXCEPTION 'Error. cannot place more than 100 items in user inventory';
    END IF;
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;
END;

CREATE TRIGGER inventory_limit BEFORE INSERT ON inventory FOR EACH ROW EXECUTE FUNCTION check_inventory_limit_size();
```

### Extra part 1

To fix the issue of request retries, we can establish a distinct table to store the UUID of each successful transaction. Here, the client is responsible for generating and storing the UUID, which is then passed on to the backend. The backend searches the table of successful transactions, attempting to locate the transaction using the supplied UUID. If the transaction is found, the backend returns a "Success" status to the client. If not, it initiates a new transaction.

### Extra part 2

To fix the issue of having numerous storage systems or multiple external CRUD services, one can establish a retry mechanism utilizing a synchronization system. This system should be robust and feature multiple replicas. It should function as a leader, managing retries in the event of failures until the entire system attains a consistent state. Additionally, the system maintains the current state of all transactions within a unique data structure, such as a table containing states.
